<?php

namespace AppBundle\Model;

use AppBundle\Entity\Club;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;

class ClubManager
{
    /** @var  EntityManager $em */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function delete(Club $club)
    {
        $club->setBorrado(true);
        $this->save($club);
    }

    /**
     * @param      $entity
     * @param bool $andFlush
     *
     * @return mixed
     */
    private function save($entity, $andFlush = true)
    {
        $this->em->persist($entity);
        if ($andFlush) {
            $this->em->flush();
        }

        return $entity;
    }

}
